import React, { Component } from 'react'
import {
    Card,
    CardBody,
    CardFooter,
    Col, 
    Form,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
  } from 'reactstrap';
import createHistory from 'history/createBrowserHistory';
import Button from '@material-ui/core/Button';
import RequestHandle from '../../components/RequestHandle';
import swal from 'sweetalert';
import AuthService from '../../components/AuthService';
const Auth = new AuthService();

const Request = new RequestHandle();
const history = createHistory();

  
export default class add extends Component {
    constructor(props) {
        super(props);
        this.state = {
          collapse: true,
          fadeIn: true,
          timeout: 300,
          formdata:'',
          visible: false,
          alerttext:'',
          fname:'',
          lname:'',
          email:'',
          contact_no:'',
          payment_status:'',
          college:'',
          course:'',
          status:'',      
          password:'',
          new_password:'',
          conform_password:'',
          ChangePassword:false
        };
        this.onChange = this.onChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);
        this.printData = this.printData.bind(this);
        this.getData = this.getData.bind(this);
        this.DeleteQuery = this.DeleteQuery.bind(this);
        this.formProfile = this.formProfile.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.ChangePasswordformProfile = this.ChangePasswordformProfile.bind(this);

      }

      onChange(e){
        this.setState({[e.target.name]:e.target.value});
      }


      componentDidMount = () => {
       
        if(this.props.history.location.state !==undefined){
          if(this.props.history.location.state.password){
          this.setState({
            ChangePassword : true});
        }
      }

        Request.RequestHandle('employer/'+Auth.getProfile().id,'GET',null,this.getData); 
      }

      getData(result){
      
        if(result.status){
          var data = result.data.results[0];
          this.setState({
            name : data.name,
            email:data.email,
            contact_no:data.contact_no,
            address:data.address,
            city:data.city,
            state:data.state,
            hr_name:data.hr_name,
            hr_email:data.hr_email,
            hr_contact_no:data.hr_contact_no,
          });
        }
      }

      deleteData(status) {

        swal({
          title: "Payment Status Change Confirmation",
          text: "Are you sure that you want to "+status+" this Student?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            this.setState({
              isLoadedData: true,  
            });
            this.DeleteQuery(status);
            this.setState({
              isLoadedData: false,  
            });
          } else {
          }
        });
      }

      DeleteQuery = (status) => {
        var that  = this;
        const statusData ={
          status:status}
        Request.RequestHandle('students/payment_status/'+this.props.match.params.id,'POST', JSON.stringify(statusData),function(result){
       
          if (result.status) {
          var btn = <div > <Button variant="contained" className="active_button" color="primary" size="small" onClick={() => { that.deleteData('not_paid') }} >paid</Button></div>
          if(status ==='not_paid'){
            btn = <div > <Button variant="contained" className="active_button" color="secondary" size="small" onClick={() => { that.deleteData('paid') }}  >not paid</Button></div>
          }
  
         
          that.setState({ payment_status:btn });
  
        }
        }); 
      }

      handleSubmit(event) {
        event.preventDefault();
        const universityData ={
          password : this.state.password,
          new_password:this.state.new_password,
          conform_password:this.state.conform_password
          }
        
        Request.RequestHandle('employer/change_password/'+Auth.getProfile().id,'POST', JSON.stringify(universityData),this.printData); 
      }

      printData(Result){
      
        if(Result.status){
          this.setState({
          password : '',
          new_password:'',
          conform_password:''
          })

            swal("Succses!", "Your information has been submitted.", "success");
        }else{
            this.setState({alerttext:Result.msg,visible:true})
        }
       }
     
    // validation(){
    //     var x = true;
    //     if(this.state.name ==""){
    //         x= false;
    //     }
    //     if(this.state.email ==""){
    //         x= false;
    //     }
    //     if(this.state.contact_no ==""){
    //         x= false;
    //     }
    //     if(this.state.website ==""){
    //         x= false;
    //     }
    //     if(this.state.year ==""){
    //         x= false;
    //     }
    //     return x;
    // }

    resetForm(){
        this.setState({
          name:'',
          email:'',
          contact_no:'',
          website:'',
          address:'',
          state:'',
          city:'',
          pin_code:'',
          hr_name:'',
          hr_email:'',
          hr_contact_no:'',
          status:'',      
        })

    }

    formProfile(){
      return(
        <Form  ref={(el) => this.myFormRef = el}  encType="multipart/form-data" className="form-horizontal">
                
        <CardBody>
            <FormGroup row >
              <Col md="2">
                <Label htmlFor="text-input">Name:</Label>
              </Col>
              <Col xs="12" md="9">
                <p>{this.state.name}</p>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col md="2">
                <Label htmlFor="email-input">E-Mail:</Label>
              </Col>
              <Col xs="12" md="4">
              <p>{this.state.email}</p>
              </Col>
              <Col md="2">
                <Label htmlFor="email-input">Contact Number:</Label>
              </Col>
              <Col xs="12" md="4">
              <p>{this.state.contact_no}</p>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col md="2">
                <Label htmlFor="email-input">Address:</Label>
              </Col>
              <Col xs="12" md="9">
              <p>{this.state.address}</p>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col md="2">
                <Label htmlFor="email-input">City:</Label>
              </Col>
              <Col xs="12" md="4">
              <p>{this.state.city}</p>
              </Col>
              <Col md="2">
                <Label htmlFor="email-input">State:</Label>
              </Col>
              <Col xs="12" md="4">
              <p>{this.state.state}</p>
              </Col>
            </FormGroup>
            <hr/>
            <FormGroup row>
              <Col md="3">
                <Label htmlFor="email-input">HR Name:</Label>
              </Col>
              <Col xs="12" md="9">
              <p>{this.state.hr_name}</p>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col md="3">
                <Label htmlFor="email-input">HR Contact Number:</Label>
              </Col>
              <Col xs="12" md="9">
              <p>{this.state.hr_contact_no}</p>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col md="3">
                <Label htmlFor="email-input">HR Email:</Label>
              </Col>
              <Col xs="12" md="9">
              <p>{this.state.hr_email}</p>
              </Col>
            </FormGroup>
        
        </CardBody>
        <CardFooter>
              <Button type="button" variant="contained"   onClick={()=>{this.passwordChange()}}  >Change Password</Button>
        </CardFooter>
        </Form>
      )
    }


    ChangePasswordformProfile(){
      return(
        <Form  ref={(el) => this.myFormRef = el} onSubmit={this.handleSubmit} encType="multipart/form-data" className="form-horizontal">
                
        <CardBody>
           <Alert color="light" isOpen={this.state.visible} >
           {this.state.alerttext}
          </Alert>
            <FormGroup row >
              <Col md="3">
                <Label htmlFor="text-input">Old Password:</Label>
              </Col>
              <Col xs="12" md="9">                        
              <Input type="password" id="password" name="password" value={this.state.password} onChange ={this.onChange} placeholder="Password"/>
              </Col>
            </FormGroup>
            <FormGroup row >
              <Col md="3">
                <Label htmlFor="text-input">New Password:</Label>
              </Col>
              <Col xs="12" md="9">
              <Input type="password" id="new_password" name="new_password" value={this.state.new_password} onChange ={this.onChange} placeholder="New Password"/>              
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col md="3">
                <Label htmlFor="email-input">Conform Password:</Label>
              </Col>
              <Col xs="12" md="9">
              <Input type="password" id="conform_password" name="conform_password" value={this.state.conform_password} onChange ={this.onChange} placeholder="Conform Password"/>              
              </Col>
            </FormGroup>
        </CardBody>
        <CardFooter>
              <Button type="submit" variant="contained" color="primary" className="left-margin rightbtn"  >Save</Button>
              <Button type="button" variant="contained"   onClick={()=>{this.passwordChange()}}  >Cancel</Button>              
        </CardFooter>
        </Form>
      )
    }

    passwordChange(){
      this.setState({
        password : '',
        new_password:'',
        conform_password:''
        });

      this.setState({
        ChangePassword : !this.state.ChangePassword});
    }

  render() {
    return (
        <div className="animated fadeIn">
        <div className="title-bar" id="title-cont">
                  Profile
              </div>
          <Row >
          <Col>
          <Card>
          {this.state.ChangePassword ? this.ChangePasswordformProfile() : this.formProfile()}
   
              </Card>
              </Col>
          </Row>
          <div>
        </div>
        </div>
    )
  }
}
