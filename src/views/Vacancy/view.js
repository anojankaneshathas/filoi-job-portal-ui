import React, { Component } from 'react'
import {
    Card,
    CardBody,
    CardTitle,
    CardFooter,
    Col, 
    Form,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
    FormFeedback
  } from 'reactstrap';
import createHistory from 'history/createBrowserHistory';
import Button from '@material-ui/core/Button';
import RequestHandle from '../../components/RequestHandle';
import swal from 'sweetalert';
import Select from 'react-select';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Table, Divider, Tag } from 'antd';
import "antd/dist/antd.css";
import Modal from 'react-responsive-modal';
import InputRange from 'react-input-range';
import "react-input-range/lib/css/index.css";
import "react-tabs/style/react-tabs.css";
import FilterListIcon from '@material-ui/icons/FilterList';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { SpeedDial, BubbleList, BubbleListItem } from 'react-speed-dial';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MoreVert from '@material-ui/icons/MoreVert';
import { blue500 } from 'material-ui/styles/colors';
import Avatar from 'material-ui/Avatar';
import IconAdd from 'material-ui/svg-icons/content/add';

const Request = new RequestHandle();
  
  const history = createHistory();
  const ColoredLine = ({ color }) => (
    <hr
        style={{ 
            color: color,
            backgroundColor: color,
            height: 5
        }}
    />
  );
  
  const columns = [{
    title: 'Name',
    dataIndex: 'fname',
    key: 'fname',
  }, {
    title: 'College',
    dataIndex: 'college',
    key: 'college',
  }, {
    title: 'Course',
    dataIndex: 'course',
    key: 'course',
  },{
    title: 'Specializations',
    dataIndex: 'specialization_major',
    key: 'specialization_major',
  },{
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
  }];


  const columns2 = [{
    title: 'Name',
    dataIndex: 'fname',
    key: 'fname',
    width: '15%',
  }, {
    title: 'College',
    dataIndex: 'college',
    key: 'college',
  }, {
    title: 'Course',
    dataIndex: 'course',
    key: 'course',
  },{
    title: 'Specializations',
    dataIndex: 'specialization_major',
    key: 'specialization_major',
  },{
    title: 'Change Status',
    dataIndex: 'action',
    key: 'action',
  },{
    title: 'Other Info',
    dataIndex: 'interview',
    key: 'interview',
  }];


  const base_status = [{
    label: 'Interview invite Pending',
    value: 1,
  },
  {
    label: 'Interview invite Accepted',
    value: 2,
  },
  {
    label: 'Interview invite Rejected',
    value: 3,
  },
  {
    label: 'Interview Rejected',
    value: 4,
  },
  {
    label: 'Offer Request Pending',
    value: 5,
  },
  {
    label: 'Offer Request Rejected',
    value: 6,
  },
  {
    label: 'Offer Given',
    value: 7,
  }];

  const status = [{
    label: 'Interview invite Pending',
    value: 1,
  },
  // {
  //   label: 'Interview invite Accepted',
  //   value: 2,
  // },
  // {
  //   label: 'Interview invite Regected',
  //   value: 3,
  // },
  {
    label: 'Interview Rejected',
    value: 4,
  },
  {
    label: 'Offer Request Pending',
    value: 5,
  },
  // {
  //   label: 'Offer Request Rejected',
  //   value: 6,
  // },
  {
    label: 'Offer Given',
    value: 7,
  }];

  


  var style = {
    cursor:'not-allowed'
  };
 

export default class add extends Component {
    constructor(props) {
        super(props);
        this.state = {
          collapse: true,
          fadeIn: true,
          timeout: 300,
          formdata:'',
          visible: false,
          alerttext:'',
          job_title:'',
          number:'',
          description:'',
          closing_date:'', 
          specialization:'',     
          status:'active', 
          universityOption:[],
          selectedOption: [],
          selectedArry:[],
          spOptions:[], 
          tabIndex: 0,
          editInterview:false,
          date_from:'',
          venue:'',
          suggestionData:[],   
          suggestionPrintData:[],
          suggestionAcceptData:[],   
          suggestionAcceptPrintData:[],
          initilSuggestionAcceptData:[],
          interviewData:[],   
          interviewPrintData:[],
          interviewAcceptData:[],   
          interviewAcceptPrintData:[],
          open: false,
          fname:'',
          lname:'',
          email:'',
          gender:'',
          course:'',
          college:'',
          desired_educational_qualification:'',     
          job_location:'',   
          job_start_date:'',     
          desired_skills:'',     
          minimum_marks_required:'',
          studentDetails:[],
          filter_open:false ,
          searchUni:'',
          searchageFrom:'',     
          searchageTo:'',     
          searchMarks:'',     

          searchUniversity:[],
          universityFilter:[],

          searchCollege:[],
          collegeFilter:[],

          searchCourse:[],
          courseFilter:[],

          searchSpecializations:[],
          specializationsFilter:[],

          searchGender:'',
          value: { min: 18, max: 100 },

          graduated_year:'',
          min_overall_grade:'',
          openStatusModel:false,
          change_status:[],
          selected_status:'',
          selected_status_student_id:'',
          search_status:'',
          intrview_location:'',
          intrview_date:'',
          openStatusInterviewModel:false,
          openStatusOfferModel:false
        };
        this.onChange = this.onChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);
        this.printData = this.printData.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.changeSpecializations = this.changeSpecializations.bind(this);
        this.getData = this.getData.bind(this);
        this.tagSelect = this.tagSelect.bind(this);
        this.getSuggestion = this.getSuggestion.bind(this);
        this.setInviteTable = this.setInviteTable.bind(this);
        this.setAcceptTable = this.setAcceptTable.bind(this);
        this.viewStudent = this.viewStudent.bind(this);
        this.viewAcceptStudent = this.viewAcceptStudent.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
        this.onOpenModal = this.onOpenModal.bind(this);
        this.getAccepted = this.getAccepted.bind(this);
        this.actionInterview = this.actionInterview.bind(this);
        this.getInterviewAccepted = this.getInterviewAccepted.bind(this);
        this.setInterviewAcceptTable = this.setInterviewAcceptTable.bind(this);
        this.actionOffer = this.actionOffer.bind(this);
        this.filterModel = this.filterModel.bind(this);
        this.handleChangeUniversityFilter = this.handleChangeUniversityFilter.bind(this);
        this.changeUniversityFilter = this.changeUniversityFilter.bind(this);

        this.handleChangeCollegeFilter = this.handleChangeCollegeFilter.bind(this);
        this.changeCollegeFilter = this.changeCollegeFilter.bind(this);

        this.handleChangeCourseFilter = this.handleChangeCourseFilter.bind(this);
        this.changeCourseFilter = this.changeCourseFilter.bind(this);

        this.handleChangeSpecializationsFilter = this.handleChangeSpecializationsFilter.bind(this);
        this.changeSpecializationsFilter = this.changeSpecializationsFilter.bind(this);

        this.handleChangeGenderFilter = this.handleChangeGenderFilter.bind(this);
        this.filterSubmit = this.filterSubmit.bind(this);
        this.isOpenStatusPopup = this.isOpenStatusPopup.bind(this);
        this.handleChangeStatus = this.handleChangeStatus.bind(this);
        this.updateStatus = this.updateStatus.bind(this);
        this.initialSelectedStudent = this.initialSelectedStudent.bind(this);
        this.handleChangeSearchStatus = this.handleChangeSearchStatus.bind(this);
        this.filterSelectedStudent = this.filterSelectedStudent.bind(this);
        this.SearchWithState = this.SearchWithState.bind(this);
        this.statusInterviewChange = this.statusInterviewChange.bind(this);
        this.filterButton = this.filterButton.bind(this);
        this.statusOfferChange = this.statusOfferChange.bind(this);
        
      }

      handleChangeUniversityFilter = (selectedOption) => {
        this.setState({ searchUniversity:selectedOption });
      }

      handleChangeSearchStatus = (selectedOption) => {
        this.setState({ search_status:selectedOption });
        
         }

      SearchWithState(){
        this.filterSelectedStudent(this.state.search_status);
      }

      changeUniversityFilter(inputValue){
        var that =this;
        Request.RequestHandle('university?search='+inputValue,'GET',null,function(result) {
          that.setState({universityFilter:[]});          
          var universityFilter =[];
              result.data.results.map(item => { 
                universityFilter.push({
                  'label':item.name,
                  'value':item._id,
                  });
              });
              that.setState({universityFilter});
          }); 
      }

      handleChangeCollegeFilter = (selectedOption) => {
        this.setState({ searchCollege:selectedOption });
      }

      changeCollegeFilter(inputValue){
        var that =this;
        Request.RequestHandle('college?search='+inputValue,'GET',null,function(result) {
          console.log('====================================');
          console.log(result);
          console.log('====================================');
          if(result.status){
            that.setState({collegeFilter:[]});          
            var collegeFilter =[];
                result.data.results.map(item => { 
                  collegeFilter.push({
                    'label':item.name,
                    'value':item._id,
                    });
                });
                that.setState({collegeFilter});
          }
          }); 
      }

     

      handleChangeCourseFilter = (selectedOption) => {
        this.setState({ searchCourse:selectedOption });
      }

      changeCourseFilter(inputValue){
        var that =this;
        Request.RequestHandle('course?search='+inputValue,'GET',null,function(result) {
          that.setState({courseFilter:[]});          
          var courseFilter =[];
              result.data.results.map(item => { 
                courseFilter.push({
                  'label':item.name,
                  'value':item._id,
                  });
              });
              that.setState({courseFilter});
          }); 
      }


      handleChangeSpecializationsFilter = (selectedOption) => {
        this.setState({ searchSpecializations:selectedOption });
      }

      changeSpecializationsFilter(inputValue){
        var that =this;
        Request.RequestHandle('specializations?search='+inputValue,'GET',null,function(result) {
          console.log('====================================');
          console.log(result);
          console.log('====================================');
          if(result.status){
            that.setState({specializationsFilter:[]});          
            var specializationsFilter =[];
                result.data.results.map(item => { 
                  specializationsFilter.push({
                    'label':item.name,
                    'value':item._id,
                    });
                });
                that.setState({specializationsFilter});
          }
          }); 
      }

      handleChangeGenderFilter = (selectedOption) => {
        this.setState({ searchGender:selectedOption });
      }

      handleChangeStatus = (selectedOption) => {
        this.setState({ selected_status:selectedOption });
      }

      onOpenModal = () => {
       this.setState({ open: true});
      };

      isOpenStatusPopup(){
       this.setState({ openStatusModel: true});
      }

      onFilterOpenModal = () => {
        this.setState({ filter_open: true});
       };


      onCloseModal = () => {
        this.setState({ open: false });
        this.setState({ filter_open: false});
        this.setState({ openStatusModel: false});
        this.setState({ openStatusInterviewModel: false});
        this.setState({ openStatusOfferModel: false});

      };

      componentDidMount = () => {
        Request.RequestHandle('vacancy/'+this.props.match.params.id,'GET',null,this.getData); 
        Request.RequestHandle('employer/suggestion/'+this.props.match.params.id,'POST',null,this.getSuggestion); 
        this.initialSelectedStudent();
       
      }


      filterButton() {
      if (this.state.tabIndex ===1) {
        let that =this;
        const list =  {
                    items: [{
                        primaryText: 'Filter',
                        rightAvatar: <Avatar backgroundColor={blue500} icon={<FilterListIcon />} />,
                        onClick() {
                          that.onFilterOpenModal()
                        },
                      }]
                    };
      
        const listbutton =<BubbleList>
                          <BubbleListItem {...list.items[0]} />
                          </BubbleList>;
      
      
        const floatingActionButtonProps =
                            {
                            backgroundColor: blue500
                            };
  
          return (
              <MuiThemeProvider>
              <SpeedDial
              hasBackdrop={false}
              icon={<MoreVert />}
              floatingActionButtonProps={floatingActionButtonProps}
              className ="flotingBtn"
              onChange ={()=>{this.onFilterOpenModal()}}
              >
              {listbutton}
              </SpeedDial>
              </MuiThemeProvider>
                );
       
      }
      else{
        return "";
      }
    }

      initialSelectedStudent(){
        this.setState({search_status:''})
        Request.RequestHandle('vacancy/'+this.props.match.params.id+'/candidates','GET',null,this.getAccepted); 
      }

      filterSelectedStudent(status_id){
        //this.setState({suggestionAcceptData:[]})
        console.log('====================================');
        console.log(status_id);
        console.log('====================================');
        Request.RequestHandle('vacancy/'+this.props.match.params.id+'/candidates?status='+status_id.value,'GET',null,this.getAccepted); 
      }

      filterSubmit(){
        let searchUniversity = [];
        this.state.searchUniversity.map(item =>{
          searchUniversity.push(item.value)
        })
        let searchCollege = [];
        this.state.searchCollege.map(item =>{
          searchCollege.push(item.value)
        })
        let searchSpecializations = [];
        this.state.searchSpecializations.map(item =>{
          searchSpecializations.push(item.value)
        })
        let searchCourse = [];
        this.state.searchCourse.map(item =>{
          searchCourse.push(item.value)
        })

        var gender = "";
        if(this.state.searchGender.value !== undefined){
          gender = this.state.searchGender.value;
        }

        var searchageTo = "";
        if(this.state.value.max <= 80){
          searchageTo = this.state.value.max;
        }

        var searchageFrom = "";
        if(this.state.value.min >= 10){
          searchageTo = this.state.value.min;
        }
        
        let filter = {
          'searchUni':searchUniversity,
          'searchSpecialization':searchSpecializations,
          'searchCollege':searchCollege,
          'searchCourse':searchCourse,
          'searchGender':gender,
          'searchageFrom':searchageFrom,
          'searchageTo':searchageTo,
          'graduated_year':this.state.graduated_year,
          'searchMarks':this.state.min_overall_grade,
        }
        console.log('====================================');
        console.log(filter);
        console.log('====================================');
        Request.RequestHandle('employer/suggestion/'+this.props.match.params.id,'POST',JSON.stringify(filter),this.getSuggestion); 
      }

      getSuggestion(result){
       console.log('====================================');
       console.log(result);
       console.log('====================================');
        if(result.status){
          this.setState({suggestionData:result.data.results});
          this.setInviteTable();   
          this.onCloseModal();
        }
      }

      getAccepted(result){
        console.log('====================================');
        console.log('resultss',result);
        console.log('====================================');
        if(result.status){
        
           this.setState({suggestionAcceptData:result.data.results,initilSuggestionAcceptData:result.data.results});
           this.setAcceptTable();   
        }
      }

      getInterviewAccepted(result){
        if(result.status){
           this.setState({interviewAcceptData:result.data.results});
           this.setInterviewAcceptTable();   
        }
      }

    

      setInviteTable(){
        let suggestionPrintData =[];
        this.state.suggestionData.map((item,key) => {
          let name = <a className="link" href  onClick={()=>{this.viewAcceptStudent(item.student_doc[0]._id)}}><b>{item.student_doc[0].fname + " " + item.student_doc[0].lname}</b></a> 
          let action =  <span><a href   onClick={()=>{this.actionInvite(key,"1",item.student_doc[0]._id)}}>Invite</a><Divider type="vertical" /><a href  onClick={()=>{this.actionInvite(key,"0",item.student_doc[0]._id)}}>Delete</a></span>
          suggestionPrintData.push({key:key,fname:name,college:item.college_doc[0].name,course:item.course_doc[0].name,specialization_major:item.major_doc[0].name + ", " + item.minor_doc[0].name,action:action,})
        }); 
        this.setState({suggestionPrintData});
      }

      setAcceptTable(){
        let suggestionAcceptPrintData =[];
        this.state.suggestionAcceptData.map((item,key) => {
          let name = <a className="link" href  onClick={()=>{this.viewAcceptStudent(item.student_doc[0]._id)}}><b>{item.student_doc[0].fname + " " + item.student_doc[0].lname}</b></a> 
          let action = "";

          base_status.map((value,key) =>{
          if(value.value === parseInt(item.status)){
           action =  <span><a href   onClick={()=>{this.statusChange(item.student_doc[0]._id,key)}}>{value.label}</a></span>
            
          }
         })
         let interview_button = "";
         if(parseInt(item.status) >= 2 && parseInt(item.status) <= 4){
           interview_button = <span><a href   onClick={()=>{this.statusInterviewChange(item.student_doc[0]._id,key)}}>interview info</a></span>
         }else if(parseInt(item.status) >= 5){
          interview_button = <span><a href   onClick={()=>{this.statusOfferChange(item.student_doc[0]._id,key)}}>Offer info</a></span>
         }
         console.log('====================================');
         console.log("mhh",item.status);
         console.log('====================================');
         
          suggestionAcceptPrintData.push({key:key,fname:name,college:item.college_doc[0].name,course:item.course_doc[0].name,specialization_major:item.major_doc[0].name + ", " + item.minor_doc[0].name,action:action,interview:interview_button})
        }); 
        this.setState({suggestionAcceptPrintData});
    }


    statusChange(student_id,status_id){
     // this.setState({change_status:status});
      var new_status = [];
     
    console.log(status_id);
    console.log(base_status);
    //alert(status_id);
    status_id++;

      if(status_id === 0){
        //new_status.push(base_status[0]);
      }else if(status_id === 1){
        //new_status.push(base_status[0]);
      }else if(status_id === 2){
        new_status.push(base_status[3]);
        new_status.push(base_status[4]);
      }else if(status_id === 3){
        //new_status.push(base_status[1]);
      }else if(status_id === 4){
        //new_status.push(base_status[1]);
      }else if(status_id === 5){
        new_status.push(base_status[6]);
      }else if(status_id === 6){
      }

      if(new_status.length > 0)
      {
        this.setState({change_status:new_status,selected_status_student_id:student_id});
        this.isOpenStatusPopup();
      }
      else
      {
        swal("Info!", "There is no possible status change available for you at the moment.", "info");
      }
     
    
     }

     statusInterviewChange(){
      this.setState({openStatusInterviewModel:true});
     }

     statusOfferChange(){
      this.setState({openStatusOfferModel:true});
     }
     
     updateStatus(){
       var upset = {
         status:this.state.selected_status.value,
         student_id:this.state.selected_status_student_id,
         vacancy_id:this.props.match.params.id,
       }
       let that = this;
      Request.RequestHandle('vacancy/candidates/change_status/','POST',JSON.stringify(upset),function(result) {
        if(result.status){
          swal("Success!", "Your information has been submitted.", "success");
          that.initialSelectedStudent();
        }else{
          swal("Error!", result.msg, "error");
        }
      }); 
     }

    setInterviewAcceptTable(){
      let interviewAcceptPrintData =[];
      if(this.state.interviewAcceptData.length !==0 ){
      this.state.interviewAcceptData.map((item,key) => {
        let name = <a className="link" href  onClick={()=>{this.viewAcceptStudent(item.student_doc[0]._id)}}><b>{item.student_doc[0].fname + " " + item.student_doc[0].lname}</b></a> 
        let action =  <span><a href   onClick={()=>{this.actionOffer(key,'accept',item.student_doc[0]._id)}}>Invite</a><Divider type="vertical" /><a href  onClick={()=>{this.actionOffer(key,'reject',item.student_doc[0]._id)}}>Delete</a></span>
        interviewAcceptPrintData.push({key:key,fname:name,college:item.college_doc[0].name,course:item.course_doc[0].name,specialization_major:item.major_doc[0].name + ", " + item.minor_doc[0].name,action:action})
      }); 
      this.setState({interviewAcceptPrintData});
    }
  }
      

      actionInvite(key,action,student_id){
        let title = "invite Confirmation";
        let text = "Are you sure that you want to invite this student?";
        let icon = "success";
        if(action ==="0"){
           title = "Delete Confirmation";
           text = "Are you sure that you want to Delete this student?";
           icon = "success";
        }
        

        swal({
          title:title,
          text:text,
          icon:icon,
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {    
            var upset = {
              status:action,
              student_id:student_id,
              vacancy_id:this.props.match.params.id,
            }
            let that = this;
           Request.RequestHandle('vacancy/candidates/change_status/','POST',JSON.stringify(upset),function(result) {
             if(result.status){
              // swal("Success!", "Your information has been submitted.", "success");
               let suggestionData =that.state.suggestionData;
                delete suggestionData[key]; 
                that.setState({suggestionData});
                that.setInviteTable();
               that.initialSelectedStudent();
             }else{
               swal("Error!", result.msg, "error");
             }
           }); 
          } else {
          }
        });

        
      }

      actionInterview(key,action,student_id){
        console.log('====================================');
        console.log(key,action,student_id);
        console.log('====================================');
        var actionData={
          accept:action
        };
        let that = this;
        Request.RequestHandle('employer/vacancy/'+this.props.match.params.id+'/student/'+student_id+'/interview','POST',JSON.stringify(actionData),function(result){
          if (result.status) {
           
            let suggestionAcceptData =that.state.suggestionAcceptData;
            delete suggestionAcceptData[key]; 
            that.setState({suggestionAcceptData});
            that.setAcceptTable();
          }
        }); 
      }

      actionOffer(key,action,student_id){
        console.log('====================================');
        console.log(key,action,student_id);
        console.log('====================================');
        var actionData={
          accept:action
        };
        let that = this;
        Request.RequestHandle('employer/vacancy/'+this.props.match.params.id+'/student/'+student_id+'/offer','POST',JSON.stringify(actionData),function(result){
          if (result.status) {
           
            let interviewAcceptData =that.state.interviewAcceptData;
            delete interviewAcceptData[key]; 
            that.setState({interviewAcceptData});
            that.setAcceptTable();
          }else{
            console.log('====================================');
            console.log(result);
            console.log('====================================');
          }
        }); 
      }

      viewStudent(id){
      console.log('student',id);
      console.log('student',this.state.suggestionData);
      let suggestionData = this.state.suggestionData[id].student_doc[0];
      let college = this.state.suggestionData[id].college_doc[0];
      let course = this.state.suggestionData[id].course_doc[0];
      if(suggestionData !== undefined){
        this.setState({
          fname:suggestionData.fname,
          lname:suggestionData.lname,
          email:suggestionData.email,
          gender:suggestionData.gender,
          course:course.name,
          college:college.name,
        });
        this.onOpenModal();
      }
      }

      viewAcceptStudent(id){
        this.props.history.push({
          pathname: '/employer/student/'+id,
       })
        }

      getData(result){
        console.log(result);
        if(result.status){
          var data = result.data.results[0];


          var spOptions =[];
          data.spec_doc.map(item => { 
            spOptions.push({
              'label':item.name,
              'value':item._id,
              });
          });
          this.setState({selectedOption:spOptions});

          console.log(this.state.spOptions);

          this.setState({
            job_title:data.job_title,
            date_from:data.date_from,
            venue:data.venue,
            number:data.number,
            description:data.description,
            closing_date:data.closing_date,      
            status:data.status,  
            desired_educational_qualification:data.desired_educational_qualification,    
            job_location:data.job_location,    
            job_start_date:data.job_start_date,    
            desired_skills:data.desired_skills,    
            minimum_marks_required:data.minimum_marks_required,    
          });
        }
      }

      onChange(e){
        this.setState({[e.target.name]:e.target.value});
      }

      handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        var selectOptions = [];
        selectedOption.map(item => { 
          selectOptions.push(item.value)
        })
        this.setState({ selectedArry:selectOptions });
        console.log(`Option selected:`, selectOptions);
      }

      changeSpecializations(inputValue){
        console.log(inputValue);
        var that =this;
        Request.RequestHandle('specializations?search='+inputValue,'GET',null,function(result) {
          that.setState({universityOption:[]});              
          var spOptions =[];
              result.data.results.map(item => { 
                spOptions.push({
                  'label':item.name,
                  'value':item._id,
                  });
              });
              that.setState({spOptions});
          }); 
      }

      handleSubmit(event) {
        const universityData ={
          date_from : this.state.date_from,
          venue:this.state.venue 
          }
          Request.RequestHandle('vacancy/'+this.props.match.params.id+'/interview','POST', JSON.stringify(universityData),this.printData); 
      }

      printData(Result){
        console.log(Result);
        if(Result.status){
         // this.resetForm();
            swal("Succses!", "Your information has been submitted.", "success");
            this.setState({editInterview:false});            

        }else{
            this.setState({alerttext:Result.msg,visible:true})
        }
       }
     
    tagSelect(tagSelect){
      this.setState({tabIndex:tagSelect});
      if(tagSelect === 1){
        this.setState({footerButton:false});
      }else{
        this.setState({footerButton:true});
      }
    }

  


    filterModel(){
     return( <Modal open={this.state.filter_open} onClose={this.onCloseModal} center>
      <Row>
        <Col>
          <Card>
          <CardBody>
          {/* <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Name:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Input type="text"  id="searchName" name="searchName" value={this.state.searchName} onChange ={this.onChange} placeholder=""/>                     
                      </Col>
          </FormGroup> */}
          <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">University:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Select
                        value={this.state.searchUniversity}
                        onChange={this.handleChangeUniversityFilter}
                        onInputChange ={this.changeUniversityFilter}
                        options={this.state.universityFilter}
                        isSearchable
                        isMulti
                      />
                      </Col>
          </FormGroup>
          <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">College:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Select
                        value={this.state.searchCollege}
                        onChange={this.handleChangeCollegeFilter}
                        onInputChange ={this.changeCollegeFilter}
                        options={this.state.collegeFilter}
                        isSearchable
                        isMulti
                      />
                      </Col>
          </FormGroup>
          <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Course:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Select
                        value={this.state.searchCourse}
                        onChange={this.handleChangeCourseFilter}
                        onInputChange ={this.changeCourseFilter}
                        options={this.state.courseFilter}
                        isSearchable
                        isMulti
                      />
                      </Col>
          </FormGroup>
          <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Specialization:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Select
                        value={this.state.searchSpecializations}
                        onChange={this.handleChangeSpecializationsFilter}
                        onInputChange ={this.changeSpecializationsFilter}
                        options={this.state.specializationsFilter}
                        isSearchable
                        isMulti
                      />
                      </Col>
          </FormGroup>
          <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Gender:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Select
                        value={this.state.searchGender}
                        onChange={this.handleChangeGenderFilter}
                        options={[{
                                  'label':'Male',
                                  'value':'male',
                                  },{
                                  'label':"Female",
                                  'value':"female",
                                  }]}
                        isSearchable
                      />
                      </Col>
          </FormGroup>
          <FormGroup row>
              <Col md="3">
                <Label htmlFor="email-input">Minimum Overall Grade:</Label>
              </Col>
              <Col xs="12" md="9">
                <Input type="text"  id="min_overall_grade" name="min_overall_grade" value={this.state.min_overall_grade} onChange ={this.onChange} placeholder=""/>
              </Col>
          </FormGroup>
          <FormGroup row>
              <Col md="3">
                <Label htmlFor="email-input">Graduated Year:</Label>
              </Col>
              <Col xs="12" md="9">
                <Input type="text"  id="graduated_year" name="graduated_year" value={this.state.graduated_year} onChange ={this.onChange} placeholder=""/>
              </Col>
          </FormGroup>
          <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Age:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <InputRange
                        maxValue={100}
                        minValue={0}
                        value={this.state.value}
                        onChange={value => this.setState({ value })} />
                      </Col>
          </FormGroup>
          <hr/>
          <Button
            type="button"
            variant="contained"
            color="primary"
            className="left-margin rightbtn"
            onClick={() => {
              this.filterSubmit();
            }}
          >
            Filter
          </Button>
          <Button
            type="button"
            variant="contained"
            onClick={() => {
              this.onCloseModal();
            }}
          >
            Cancel
          </Button>
        </CardBody>
          </Card>
        </Col>
      </Row>
      </Modal>)

    }

  render() {
    const {alerttext,open} = this.state;
    const { selectedOption } = this.state;

    return (
        <div className="animated fadeIn">
        <div className="title-bar" id="title-cont">
                  Vacancy
              </div>
          <Row >
          <Col>
          <Card>  
                <CardBody className="pb-0">
          {this.filterModel()}
        <Modal open={open} onClose={this.onCloseModal} center>
        <Row>
          <Col>
            <Card>
            <CardBody  >
            <CardTitle>Student Profile</CardTitle>
          <FormGroup row>
            <Col md="2">
              <Label htmlFor="text-input">First Name:</Label>
            </Col>
            <Col xs="12" md="4">
              <p>{this.state.fname}</p>
            </Col>
            <Col md="2">
              <Label htmlFor="text-input">Last Name:</Label>
            </Col>
            <Col xs="12" md="4">
              <p>{this.state.lname}</p>
            </Col>
          </FormGroup>
          <FormGroup row>
          <Col md="2">
              <Label htmlFor="text-input">College:</Label>
            </Col>
            <Col xs="12" md="4">
            <p>{this.state.college}</p>
            </Col>
            <Col md="2">
              <Label htmlFor="text-input">Course:</Label>
            </Col>
            <Col xs="12" md="4">
            <p>{this.state.course}</p>
            </Col>
          </FormGroup>
          <FormGroup row>
          <Col md="2">
              <Label htmlFor="text-input">College:</Label>
            </Col>
            <Col xs="12" md="4">
            <p>{this.state.college}</p>
            </Col>
            <Col md="2">
              <Label htmlFor="text-input">Course:</Label>
            </Col>
            <Col xs="12" md="4">
            <p>{this.state.course}</p>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col md="2">
              <Label htmlFor="text-input">Email:</Label>
            </Col>
            <Col xs="12" md="4">
            <p>{this.state.email}</p>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col md="2">
              <Label htmlFor="text-input">Permanent Address:</Label>
            </Col>
            <Col xs="12" md="4">
            <p className="qution">{this.state.studentDetails.permanant_address},</p>
            <p className="qution">{this.state.studentDetails.p_city},</p>
            <p className="qution">{this.state.studentDetails.p_state},</p>
            <p className="qution">{this.state.studentDetails.p_pin_code}.</p>
            </Col>
            <Col md="2">
              <Label htmlFor="text-input">Current Address:</Label>
            </Col>
            <Col xs="12" md="4">
            <p className="qution">{this.state.studentDetails.current_address},</p>
            <p className="qution">{this.state.studentDetails.c_city},</p>
            <p className="qution">{this.state.studentDetails.c_state},</p>
            <p className="qution">{this.state.studentDetails.c_pin_code}.</p>
            </Col>
          </FormGroup>
         
          <hr/>
          <Button
            type="button"
            variant="contained"
            color="primary"
            className="left-margin rightbtn"
            onClick={() => {
              this.writtenExamSave();
            }}
          >
            Invite
          </Button>
          <Button
            type="button"
            variant="contained"
            onClick={() => {
              this.onCloseModal();
            }}
          >
            Cancel
          </Button>
          </CardBody>
            </Card>
          </Col>
        </Row>
        </Modal>


        <Modal open={this.state.openStatusModel} onClose={this.onCloseModal} center>
        <Row>
          <Col>
            <Card>
            <CardBody  >
            <CardTitle>Status Change</CardTitle>
            <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Status:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Select
                        value={this.state.selected_status}
                        onChange={this.handleChangeStatus}
                        options={this.state.change_status}
                        isSearchable
                      />
                      </Col>
          </FormGroup>
          <hr/>
          <Button
            type="button"
            variant="contained"
            color="primary"
            className="left-margin rightbtn"
            onClick={() => {
              this.updateStatus();
            }}
          >
            Update
          </Button>
          <Button
            type="button"
            variant="contained"
            onClick={() => {
              this.onCloseModal();
            }}
          >
            Cancel
          </Button>
          </CardBody>
            </Card>
          </Col>
        </Row>
        </Modal>


        <Modal open={this.state.openStatusInterviewModel} onClose={this.onCloseModal} center>
        <Row>
          <Col>
            <Card>
            <CardBody  >
            <CardTitle>Add Interview</CardTitle>
            <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Venue:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Input 
                      type="text" 
                      id="intrview_location" 
                      name="intrview_location" 
                      value={this.state.intrview_location} 
                      onChange ={this.onChange} 
                      placeholder=""/>
                      
                      </Col>
          </FormGroup>
          <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Date:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Input 
                      type="date" 
                      id="intrview_date" 
                      name="intrview_date" 
                      value={this.state.intrview_date} 
                      onChange ={this.onChange} 
                      placeholder=""/>
                      
                      </Col>
          </FormGroup>
          <hr/>
          <Button
            type="button"
            variant="contained"
            color="primary"
            className="left-margin rightbtn"
            onClick={() => {
              this.onCloseModal();
            }}
          >
            Update
          </Button>
          <Button
            type="button"
            variant="contained"
            onClick={() => {
              this.onCloseModal();
            }}
          >
            Cancel
          </Button>
          </CardBody>
            </Card>
          </Col>
        </Row>
        </Modal>

        <Modal open={this.state.openStatusOfferModel} onClose={this.onCloseModal} center>
        <Row>
          <Col>
            <Card>
            <CardBody  >
            <CardTitle>Offer Info</CardTitle>
            <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Job Start Date:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Input 
                      type="date" 
                      id="job_start_date" 
                      name="job_start_date" 
                      value={this.state.job_start_date} 
                      onChange ={this.onChange} 
                      placeholder=""/>
                      
                      </Col>
          </FormGroup>
          <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Annual CTC(Amount):</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Input 
                      type="text" 
                      id="annual_ctc" 
                      name="annual_ctc" 
                      value={this.state.annual_ctc} 
                      onChange ={this.onChange} 
                      placeholder=""/>
                      
                      </Col>
          </FormGroup>
          <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Job Location:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Input 
                      type="text" 
                      id="job_location_offer" 
                      name="job_location_offer" 
                      value={this.state.job_location_offer} 
                      onChange ={this.onChange} 
                      placeholder=""/>
                      
                      </Col>
          </FormGroup>
          <hr/>
          <Button
            type="button"
            variant="contained"
            color="primary"
            className="left-margin rightbtn"
            onClick={() => {
              this.onCloseModal();
            }}
          >
            Update
          </Button>
          <Button
            type="button"
            variant="contained"
            onClick={() => {
              this.onCloseModal();
            }}
          >
            Cancel
          </Button>
          </CardBody>
            </Card>
          </Col>
        </Row>
        </Modal>


                <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.tagSelect(tabIndex)}>
                  <TabList>
                    <Tab>Basic Details</Tab>
                    <Tab>Shortlist Students</Tab>
                    <Tab>Invited Students</Tab>
                  </TabList>
                  <TabPanel>
                    <FormGroup row >
                      <Col md="3">
                        <Label htmlFor="text-input">Job Title:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.job_title}</p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">No of Position:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.number}</p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Specializations:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>
                      {this.state.selectedOption.map((material,index) => { 
                        return material.label+', '
                      })
                      }
                      </p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Start Date:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.job_start_date}</p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Closing Date:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.closing_date}</p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Location:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.job_location}</p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Request Skills:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.desired_skills}</p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Desired Education Qualification:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.desired_educational_qualification}</p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Minimum Marks:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.minimum_marks_required}</p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Description:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.description}</p>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Status:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <p>{this.state.status}</p>
                      </Col>
                    </FormGroup>
                    </TabPanel>
                  <TabPanel>
                  <Table columns={columns} dataSource={this.state.suggestionPrintData} />
                  </TabPanel>
                  <TabPanel>
                  <div className="">
                      <Row>
                      <Col xs="12" md="2">
                      </Col>
                      <Col xs="12" md="5">
                      <Select
                        value={this.state.search_status}
                        onChange={this.handleChangeSearchStatus}
                        options={base_status}
                        placeholder ={"Filter by Status"}
                      />
                      </Col>
                      <Col md="4">
                      <Button
                        type="button"
                        variant="contained"
                        color="primary"
                        onClick={() => {
                          this.SearchWithState();
                        }}
                      >
                        Filter
                      </Button>
                      <Button
                        type="button"
                        variant="contained"
                        className = "clear_button"
                        onClick={() => {
                          this.initialSelectedStudent();
                        }}
                      >
                        Clear
                      </Button>
                      
                      </Col>
                      </Row>
                  </div>
                  <hr/>
                  {this.state.suggestionAcceptPrintData.length === 0 ?
                  <div>No data found</div>
                  :
                  <Table columns={columns2} dataSource={this.state.suggestionAcceptPrintData} />
                  }
                  </TabPanel>
                </Tabs>
                {this.filterButton()}
                </CardBody>
              </Card>
              </Col>
          </Row>
          <div>
        </div>
        </div>
    )
  }
}