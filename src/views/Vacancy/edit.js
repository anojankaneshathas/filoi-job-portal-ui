import React, { Component } from 'react'
import {
    Card,
    CardBody,
    CardFooter,
    Col, 
    Form,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
    FormFeedback
  } from 'reactstrap';
import createHistory from 'history/createBrowserHistory';
import Button from '@material-ui/core/Button';
import RequestHandle from '../../components/RequestHandle';
import swal from 'sweetalert';
import Select from 'react-select';

const Request = new RequestHandle();
  
  const history = createHistory();
  const ColoredLine = ({ color }) => (
    <hr
        style={{ 
            color: color,
            backgroundColor: color,
            height: 5
        }}
    />
  );
  
 

export default class add extends Component {
    constructor(props) {
        super(props);
        this.state = {
          collapse: true,
          fadeIn: true,
          timeout: 300,
          formdata:'',
          visible: false,
          alerttext:'',
          job_title:'',
          number:'',
          description:'',
          closing_date:'', 
          specialization:'',     
          status:'active', 
          universityOption:[],
          selectedOption: null,
          selectedArry:[],
          spOptions:[],
          desired_educational_qualification:'',     
          job_location:'',   
          job_start_date:'',     
          desired_skills:'',     
          minimum_marks_required:'',          
        };
        this.onChange = this.onChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);
        this.printData = this.printData.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.changeSpecializations = this.changeSpecializations.bind(this);
        this.getData = this.getData.bind(this);

      }

      componentDidMount = () => {
        Request.RequestHandle('vacancy/'+this.props.match.params.id,'GET',null,this.getData); 
      }

      getData(result){
        console.log(result);
        if(result.status){
          var data = result.data.results[0];


          var spOptions =[];
          data.spec_doc.map(item => { 
            spOptions.push({
              'label':item.name,
              'value':item._id,
              });
          });
          this.setState({selectedOption:spOptions});

          console.log(this.state.spOptions);

          this.setState({
            job_title:data.job_title,
            number:data.number,
            description:data.description,
            closing_date:data.closing_date,    
            status:data.status,  
            desired_educational_qualification:data.desired_educational_qualification,    
            job_location:data.job_location,    
            job_start_date:data.job_start_date,    
            desired_skills:data.desired_skills,    
            minimum_marks_required:data.minimum_marks_required,    
          });
        }
      }

      onChange(e){
        this.setState({[e.target.name]:e.target.value});
      }

      handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        var selectOptions = [];
        selectedOption.map(item => { 
          selectOptions.push(item.value)
        })
        this.setState({ selectedArry:selectOptions });
        console.log(`Option selected:`, selectOptions);
      }

      changeSpecializations(inputValue){
        console.log(inputValue);
        var that =this;
        Request.RequestHandle('specializations?search='+inputValue,'GET',null,function(result) {
          that.setState({universityOption:[]});              
          var spOptions =[];
              result.data.results.map(item => { 
                spOptions.push({
                  'label':item.name,
                  'value':item._id,
                  });
              });
              that.setState({spOptions});
          }); 
      }

      handleSubmit(event) {
        event.preventDefault();
        const universityData ={
          job_title : this.state.job_title,
          number:this.state.number,
          description:this.state.description,
          closing_date:this.state.closing_date,
          status:this.state.status,
          list:this.state.selectedArry,
          desired_educational_qualification:this.state.desired_educational_qualification,    
          job_location:this.state.job_location,    
          job_start_date:this.state.job_start_date,    
          desired_skills:this.state.desired_skills,    
          minimum_marks_required:this.state.minimum_marks_required, 
          }
          Request.RequestHandle('vacancy/'+this.props.match.params.id,'POST', JSON.stringify(universityData),this.printData); 
      }

      printData(Result){
        console.log(Result);
        if(Result.status){
          this.resetForm();
            swal("Succses!", "Your information has been submitted.", "success");
            window.location.assign("/#/vacancy");

        }else{
            this.setState({alerttext:Result.msg,visible:true})
        }
       }
     
    // validation(){
    //     var x = true;
    //     if(this.state.name ==""){
    //         x= false;
    //     }
    //     if(this.state.email ==""){
    //         x= false;
    //     }
    //     if(this.state.contact_no ==""){
    //         x= false;
    //     }
    //     if(this.state.website ==""){
    //         x= false;
    //     }
    //     if(this.state.year ==""){
    //         x= false;
    //     }
    //     return x;
    // }

    resetForm(){
        this.setState({
          job_title:'',
          number:'',
          description:'',
          closing_date:'',      
          status:'active',
        })

    }

  render() {
    const {alerttext} = this.state;
    const { selectedOption } = this.state;

    return (
        <div className="animated fadeIn">
        <div className="title-bar" id="title-cont">
                  Add Vacancy
              </div>
          <Row >
          <Col>
          <Card>
          
          <Form  ref={(el) => this.myFormRef = el} onSubmit={this.handleSubmit} encType="multipart/form-data" className="form-horizontal">
                
                <CardBody>
                   <Alert color="light" isOpen={this.state.visible} >
                   {alerttext}
                  </Alert>
                    <FormGroup row >
                      <Col md="3">
                        <Label htmlFor="text-input">Job Title:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="job_title" name="job_title" value={this.state.job_title} onChange ={this.onChange}  placeholder=""  />
                        <FormFeedback>Sorry. This name is already taken.</FormFeedback>

                      </Col>
                    </FormGroup>
                   
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">No of Position:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text"  id="number" name="number" value={this.state.number} onChange ={this.onChange} placeholder=""/>
                        
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Specializations:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Select
                        value={selectedOption}
                        defaultValue={this.state.spOptions}
                        onChange={this.handleChange}
                        onInputChange ={this.changeSpecializations}
                        options={this.state.spOptions}
                        isMulti
                        isSearchable
                      />
                      </Col>
                    </FormGroup> 
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Start Date:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="date" id="job_start_date" name="job_start_date" value={this.state.job_start_date} onChange ={this.onChange} placeholder=""/>
                       
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Closing Date:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="date" id="closing_date" name="closing_date" value={this.state.closing_date} onChange ={this.onChange} placeholder=""/>
                       
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Location:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="job_location" name="job_location" value={this.state.job_location} onChange ={this.onChange} placeholder=""/>
                       
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Request Skills:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="desired_skills" name="desired_skills" value={this.state.desired_skills} onChange ={this.onChange} placeholder=""/>
                       
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Desired Education Qualification:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="desired_educational_qualification" name="desired_educational_qualification" value={this.state.desired_educational_qualification} onChange ={this.onChange} placeholder=""/>
                       
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Minimum Marks:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="minimum_marks_required" name="minimum_marks_required" value={this.state.minimum_marks_required} onChange ={this.onChange} placeholder=""/>
                       
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Description:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="description" name="description" value={this.state.description} onChange ={this.onChange} placeholder=""/>
                       
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Status:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Input type="select" id="status" name="status" value={this.state.status} onChange ={this.onChange} >
                      <option value="active">Active</option>
                        <option value="inactive">Inactive</option>
                      </Input>
                      </Col>
                    </FormGroup>
                </CardBody>
                <CardFooter>
                      <Button type="reset" variant="contained" color="secondary" className="left-margin" onClick={()=>{this.resetForm()}}  >Reset</Button>
                      <Button type="submit" variant="contained" color="primary" className="left-margin rightbtn"  >Save</Button>
                      <Button type="button" variant="contained"  className="left-margin rightbtn" onClick={()=>{history.goBack()}}  >Cancel</Button>
  
                </CardFooter>
                </Form>
              </Card>
              </Col>
          </Row>
          <div>
        </div>
        </div>
    )
  }
}
