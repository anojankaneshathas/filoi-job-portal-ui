import React, { Component } from 'react'
import {
    Card,
    CardBody,
    CardFooter,
    Col, 
    Form,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
    FormFeedback
  } from 'reactstrap';
import createHistory from 'history/createBrowserHistory';
import Button from '@material-ui/core/Button';
import RequestHandle from '../../components/RequestHandle';
import swal from 'sweetalert'
const Request = new RequestHandle();
  
  const history = createHistory();
  const ColoredLine = ({ color }) => (
    <hr
        style={{ 
            color: color,
            backgroundColor: color,
            height: 5
        }}
    />
  );
  
export default class add extends Component {
    constructor(props) {
        super(props);
        this.state = {
          collapse: true,
          fadeIn: true,
          timeout: 300,
          formdata:'',
          visible: false,
          alerttext:'',
          name:'',
          email:'',
          contact_no:'',
          website:'',
          address:'',
          state:'',
          city:'',
          pin_code:'',
          hr_name:'',
          hr_email:'',
          hr_contact_no:'',
          status:'active', 
          description:'',
          industry:'',
          location:''     
        };
        this.onChange = this.onChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);
        this.printData = this.printData.bind(this);
        //this.onDismiss = this.onDismiss.bind(this);
      }

      onChange(e){
        this.setState({[e.target.name]:e.target.value});
      }

      handleSubmit(event) {
        event.preventDefault();
        const universityData ={
            name : this.state.name,
            email:this.state.email,
            contact_no:this.state.contact_no,
            website:this.state.website,
            address:this.state.address,
            state:this.state.state,
            city:this.state.city,
            pin_code:this.state.pin_code,
            hr_name:this.state.hr_name,
            hr_email:this.state.hr_email,
            hr_contact_no:this.state.hr_contact_no,
            status:this.state.status,
            description:this.state.description,
            industry:this.state.industry,
            location:this.state.location,
          }
        
     
        Request.RequestHandle('employer','POST', JSON.stringify(universityData),this.printData); 
      }

      printData(Result){
     
        if(Result.status){
          this.setState({
            name:'',
            email:'',
            contact_no:'',
            website:'',
            address:'',
            state:'',
            city:'',
            pin_code:'',
            hr_name:'',
            hr_email:'',
            hr_contact_no:'',
            description:'',
            industry:'',
            location:'',
            status:'active',      
          })

            swal("Succses!", "Your information has been submitted.", "success");
            window.location.assign("/#/employee");

        }else{
            this.setState({alerttext:Result.msg,visible:true})
        }
       }
     
  

    resetForm(){
        this.setState({
          name:'',
          email:'',
          contact_no:'',
          website:'',
          address:'',
          state:'',
          city:'',
          pin_code:'',
          hr_name:'',
          hr_email:'',
          hr_contact_no:'',
          description:'',
          status:'active',  
          industry:'',
          location:'',    
        })

    }

  render() {
    const {alerttext} = this.state;
    return (
        <div className="animated fadeIn">
        <div className="title-bar" id="title-cont">
                  Add Employer
              </div>
          <Row >
          <Col>
          <Card>
          
          <Form  ref={(el) => this.myFormRef = el} onSubmit={this.handleSubmit} encType="multipart/form-data" className="form-horizontal">
                
                <CardBody>
                   <Alert color="light" isOpen={this.state.visible} >
                   {alerttext}
                  </Alert>
                    <FormGroup row >
                      <Col md="3">
                        <Label htmlFor="text-input">Name:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="name" name="name" value={this.state.name} onChange ={this.onChange}  placeholder=" Name"  />
                        <FormFeedback>Oh noes! that name is already taken</FormFeedback>

                      </Col>
                    </FormGroup>
                   
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">E-Mail:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text"  id="email" name="email" value={this.state.email} onChange ={this.onChange} placeholder="E-Mail" autoComplete="email"/>
                        
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Phone Number:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="contact_no" name="contact_no" value={this.state.contact_no} onChange ={this.onChange} placeholder="Phone Number" />
                       
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Web Site:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="website" name="website" value={this.state.website} onChange ={this.onChange} placeholder="URL"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Address:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="address" name="address" value={this.state.address} onChange ={this.onChange} placeholder="Address"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">State:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="state" name="state" value={this.state.state} onChange ={this.onChange} placeholder="State"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">City:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="city" name="city" value={this.state.city} onChange ={this.onChange} placeholder="City"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Pin Code:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="pin_code" name="pin_code" value={this.state.pin_code} onChange ={this.onChange} placeholder="Pin Code"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Location:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="location" name="location" value={this.state.location} onChange ={this.onChange} placeholder="Location"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Industry:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="industry" name="industry" value={this.state.industry} onChange ={this.onChange} placeholder="Industry"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">HR Name:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="hr_name" name="hr_name" value={this.state.hr_name} onChange ={this.onChange} placeholder="HR Name"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">HR Email:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="hr_email" name="hr_email" value={this.state.hr_email} onChange ={this.onChange} placeholder="HR Email"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">HR Contact No:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="text" id="hr_contact_no" name="hr_contact_no" value={this.state.hr_contact_no} onChange ={this.onChange} placeholder="HR Contact No"/>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Status:</Label>
                      </Col>
                      <Col xs="12" md="9">
                      <Input type="select" id="status" name="status" value={this.state.status} onChange ={this.onChange} >
                      <option value="active">Active</option>
                        <option value="inactive">Inactive</option>
                      </Input>
                      </Col>
                    </FormGroup>   
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="email-input">Description:</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="textarea" id="description" name="description" value={this.state.description} onChange ={this.onChange} placeholder="Description"/>
                      </Col>
                    </FormGroup>
                </CardBody>
                <CardFooter>
                      <Button type="reset" variant="contained" color="secondary" className="left-margin" onClick={()=>{this.resetForm()}}  >Reset</Button>
                      <Button type="submit" variant="contained" color="primary" className="left-margin rightbtn"  >Save</Button>
                      <Button type="button" variant="contained"  className="left-margin rightbtn" onClick={()=>{history.goBack()}}  >Cancel</Button>
  
                </CardFooter>
                </Form>
              </Card>
              </Col>
          </Row>
          <div>
        </div>
        </div>
    )
  }
}
